def get_simple_api_response(message):
    return {
        "messageCodeTO": {
            "status": "SUCCESS",
            "code": "S100",
            "displayMsg": message
        }
    }


def get_cards_api_response(title, cards):
    final_layout = {
        "messageCodeTO": {
            "code": "EC200",
            "status": "Success",
            "message": "Request accepted successfully.",
            "data": None
        },
        "data": {
            "FAQ": {
                "code": "EC200",
                "status": "Success",
                "message": "Recommended courses for you.",
                "data": [
                    {
                        "design": {
                            "label": title,
                            "type": "TEXT",
                            "subType": None,
                            "displayDataType": None,
                            "selectionType": None,
                            "questionID": None,
                            "tableTitle": None,
                            "tableSubTitle": None,
                            "sequence": None,
                            "category": None,
                            "messageId": None,
                            "dataArray": None,
                            "uiArray": None,
                            "displaySeq": None,
                            "cellTrackerID": None,
                            "selectionOptions": [],
                            "selectedValue": [],
                            "flow": None
                        }
                    },
                    {
                        "design": {
                            "label": None,
                            "type": "AUTOMATCH",
                            "subType": None,
                            "displayDataType": None,
                            "selectionType": None,
                            "questionID": None,
                            "tableTitle": None,
                            "tableSubTitle": None,
                            "sequence": None,
                            "category": None,
                            "messageId": None,
                            "dataArray": [],
                            "uiArray": None,
                            "displaySeq": "score,imgProfileUrl,candidateName,currentCompany,viewAvailable,viewUrl,tagAvailable,location,designation,tagUrl,candidateId,viewUrls",
                            "cellTrackerID": None,
                            "selectionOptions": [],
                            "selectedValue": [],
                            "flow": None
                        }
                    }
                ]
            }
        }
    }

    layout_inner = {
        "score": None,
        "imgProfileUrl": None,
        "candidateName": None,
        "currentCompany": None,
        "viewAvailable": True,
        "viewUrl": None,
        "tagAvailable": False,
        "location": None,
        "designation": None,
        "tagUrl": None,
        "candidateId": None,
        "viewUrls": []
    }

    design_array = list()
    for card in cards:
        inner_layout = layout_inner.copy()
        inner_layout["imgProfileUrl"] = card["icon"]
        inner_layout["candidateName"] = card["heading_1"]
        inner_layout["designation"] = card["heading_2"]
        inner_layout["currentCompany"] = card["heading_3"]
        inner_layout["location"] = card["footer"]
        inner_layout["viewUrls"] = card["user_click_actions"]
        design_array.append(inner_layout)

    final_layout["data"]["FAQ"]["data"][1]["design"]["dataArray"] = design_array

    return final_layout


# TODO
def get_table_api_response(theader, tbody, label="Here it is"):
    final_layout = {
        "messageCode": {
            "code": "EC200",
            "status": "Success",
            "message": "Request accepted successfully.",
            "data": None
        },
        "data": {
            "genericRequest": {
                "notification": {
                    "action": "Jinie",
                    "title": "JINIE",
                    "body": label
                },
                "text": [
                    {
                        "design": {
                            "label": label,
                            "type": "TEXT",
                            "selectionOptions": [],
                            "selectedValue": []
                        }
                    },
                    {
                        "design": {
                            "type": "Table",
                            "genericArray": [],
                            "displaySeq": "column_0,column_1",
                            "selectionOptions": [],
                            "selectedValue": []
                        }
                    }
                ]
            },
            "formName": None,
            "ssoToken": None,
            "replayDataId": None,
            "messageType": None
        }
    }


    layout_inner = {
        "column_0": None,
        "column_1": None,
    }
    generic_array = list()
    generic_array.append(theader)
    for body in tbody:
        #inner_layout = {"column_0": body[0],"column_1": body[1]}
        inner_layout = layout_inner.copy()
        inner_layout["column_0"] = body[0]
        inner_layout["column_1"] = body[1]
        generic_array.append(inner_layout)

    final_layout["data"]["genericRequest"]["text"][1]["design"]["genericArray"] = generic_array

    return final_layout



if __name__ == '__main__':
    listData = [(u'Rajive Sharma', u'Father'), (u'RENU SHARMA', u'Mother')]
    print get_table_api_response("Your Dependant details",listData)



