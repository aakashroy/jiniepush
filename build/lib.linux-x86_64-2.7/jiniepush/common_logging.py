def safe_print(to_print):
    """
    Function to safely print statements without crashing the app
    :param to_print: The string to print
    :return: None
    """
    if isinstance(to_print, unicode):
        to_print = to_print.encode("utf8")
    print to_print


def __log(log_text, show):
    """
    This methods prints log_text to the console

    :param log_text: The text to print

    :param show: Whether to display the log_text

    :return: None
    """
    if show:
        safe_print(log_text)
