def get_generic_design_layout(alt_id, text_components):
    final_layout = {
        "input": {
            "messageType": "NOTIFICATION",
            "usersList": [
                {
                    "messageType": "NOTIFICATION",
                    "userId": alt_id,
                    "text": text_components
                }
            ]
        }
    }
    return final_layout


def get_meeting_reminder_card_layout(alt_id, greeting, metg_title, metg_time, metg_loc, members, metg_actions):
    final_layout = {
        "input": {
            "messageType": "NOTIFICATION",
            "usersList": [
                {
                    "messageType": "NOTIFICATION",
                    "userId": alt_id,
                    "text": [
                        {
                            "design": [
                                {
                                    "label":greeting,
                                    "type": "OUTLOOK",
                                    "subType": "UPCOMING_MEETING",
                                    "genericArray": [
                                        {
                                            "metgTitle": metg_title,
                                            "type": "CAL",
                                            "metgTime": metg_time,
                                            "metgLoc": metg_loc,
                                            "membs": members,
                                            "metgActs": metg_actions
                                        }
                                    ],
                                    "displayDataType": None,
                                    "selectionType": None,
                                    "questionID": None,
                                    "sequence": None,
                                    "displaySeq": None,
                                    "cellTrackerID": None,
                                    "selectionOptions": None,
                                    "selectedValue": None,
                                    "flow": None
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }
    return final_layout


def get_cards_notification_layout(alt_id, title, cards):
    final_layout = {
        "input": {
            "messageType": "AUTOMATCH",
            "action": None,
            "usersList": [
                {
                    "surveyID": None,
                    "userId": alt_id,
                    "tenantId": None,
                    "organizationId": None,
                    "notificationProperty": None,
                    "action": "",
                    "messageType": None,
                    "text": [
                        {
                            "design": [
                                {
                                    "label": title,
                                    "type": "TEXT",
                                    "selectionOptions": [],
                                    "selectedValue": []
                                }
                            ]
                        },
                        {
                            "design": [{
                                "type": "AUTOMATCH",
                                "genericArray": [],
                                "displaySeq": "score,imgProfileUrl,candidateName,currentCompany,viewAvailable,viewUrl,tagAvailable,location,designation,tagUrl,candidateId,viewUrls",
                                "selectionOptions": [],
                                "selectedValue": []
                            }]
                        }
                    ]
                }
            ]
        }
    }

    _layout_inner = {
        "score": None,
        "imgProfileUrl": None,
        "candidateName": None,
        "currentCompany": None,
        "viewAvailable": True,
        "viewUrl": None,
        "tagAvailable": False,
        "location": None,
        "designation": None,
        "tagUrl": None,
        "candidateId": None,
        "viewUrls": []
    }

    design_array = list()
    for card in cards:
        inner_layout = _layout_inner.copy()
        inner_layout["imgProfileUrl"] = card["icon"]
        inner_layout["candidateName"] = card["heading_1"]
        inner_layout["designation"] = card["heading_2"]
        inner_layout["currentCompany"] = card["heading_3"]
        inner_layout["location"] = card["footer"]
        inner_layout["viewUrls"] = card["user_click_actions"]
        design_array.append(inner_layout)

    final_layout["input"]["usersList"][0]["text"][1]["design"][0]["genericArray"] = design_array

    return final_layout


def get_simple_notification_layout(alt_id, message):
    return {
        "input": {
            "messageType": "NOTIFICATION",
            "action": None,
            "usersList": [
                {
                    "surveyID": None,
                    "userId": alt_id,
                    "tenantId": None,
                    "organizationId": None,
                    "action": "",
                    "messageType": None,
                    "text": [
                        {
                            "design": [
                                {
                                    "label": message,
                                    "type": "TEXT",
                                    "subType": "",
                                    "displayDataType": "Info",
                                    "selectionType": "none",
                                    "questionID": None,
                                    "tableTitle": None,
                                    "tableSubTitle": None,
                                    "sequence": None,
                                    "dataArray": None,
                                    "genericArray": None,
                                    "displaySeq": None,
                                    "cellTrackerID": None,
                                    "action": "",
                                    "messageId": None,
                                    "selectionOptions": None,
                                    "selectedValue": [],
                                    "flow": None
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }


# TODO
def get_table_notification_layout(alt_id, theader, tbody):
    return {
        "input": {
            "messageType": "Timesheet_Pending_Action",
            "usersList": [
                {
                    "userId": alt_id,
                    "text": [
                        {
                            "design": [
                                {
                                    "label": "",
                                    "dataArray": None,
                                    "genericArray": None,
                                    "type": "Text",
                                    "subType": "",
                                    "displayDataType": "Info",
                                    "selectionType": "none",
                                    "selectionOptions": None,
                                    "maxSelection": "",
                                    "flow": "horizontal"
                                }
                            ]
                        },
                        {
                            "design": [
                                {
                                    "label": None,
                                    # dataArray
                                    # genericArray
                                    "type": "TABLE_WITH_OPTION",
                                    "subType": "",
                                    "displayDataType": "Info",
                                    "selectionType": "none",
                                    "selectionOptions": None,
                                    "maxSelection": "",
                                    "flow": "horizontal"
                                }
                            ]
                        }
                    ],
                    "type": None,
                    "subType": None,
                    "displayDataType": None,
                    "selectionType": None,
                    "selectionOptions": None,
                    "maxSelection": None,
                    "flow": False
                }
            ],
            "message": None
        }
    }
