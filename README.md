# jiniepush

This is a wrapper for sending push notifications to Peoplestrong Alt Jinie.

Setup python distribution:
  1. Create the 3 files (setup.py, LICENSE, Readme.md)
  2. run command to generate build
        python setup.py sdist bdist_wheel (within venv)
  3. Upload to repository (git push)
  4. Check
  
Clone into pip environment: 
   pip install git+https://(#userId#)@bitbucket.org/aakashroy/jiniepush.git
    

Copyright - 2018 PeopleStrong HR Services Pvt. Ltd.

All rights reserved.
