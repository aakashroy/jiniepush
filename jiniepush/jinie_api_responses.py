from jiniepush import api_responses
from jiniepush.common_logging import __log


def get_simple(message, verbose=False):
    """

    :param alt_id: The Alt ID of the user.
    This is the Id present in the ejabberd table.
    Get it from the mobile team! messenger.mobile@peoplestrong.com

    :param message: The message to send

    :param format_message: Beautify the message a bit - put in line breaks.
    Set to false if you want the message exactly to be displayed the way you want.

    :param verbose: Set this to True to see debug messages

    :return: Simple Layout Payload
    Raises exceptions for erroneous conditions

    """
    __log("Function send_simple called with arguments:", verbose)
    __log("message: " + unicode(message), verbose)
    __log("verbose: " + unicode(verbose), verbose)

    if not isinstance(message, basestring):
        raise RuntimeError("Input argument message should be of type str or unicode")

    if message is None or len(message) == 0:
        raise RuntimeError("Input argument message is None or empty")

    __log("Input arguments are okay", verbose)

    return api_responses.get_simple_api_response(message)


def get_cards(title, card_layouts, verbose=False):
    """
    :param title: The text appearing before the card layout, i.e. the narrative.
    Example: Hi xyz, here are the relevant candidates
    :param card_layout: This is a dictionary. The keys that need to be defined are:
        {
            heading_1: First text appearing in sequence in the card layout
            heading_2: Second text appearing in sequence in the card layout
            footer: Text appearing in the bottom of the card layout
            icon: The background image / icon to be displayed in the card layout
            actions: A list of dicts. Example:
            [{"logo": "https://abc.com/xyz.png", "url":"https://xyz.com"}]
            This will display an action button with the icon in "logo". On clicking the url
            will be opened in the browser
        }
    :param verbose: Set this to True to see debug messages
    :return: Card Layout Response Payload
    Raises exceptions for erroneous conditions
    """

    __log("Function send_card called with arguments:", verbose)
    __log("title: " + unicode(title), verbose)
    __log("card_layout: " + unicode(card_layouts), verbose)

    if not isinstance(title, basestring):
        raise RuntimeError("Input argument title should be of type str or unicode")

    if not isinstance(card_layouts, list):
        raise RuntimeError("Input argument card_layout should be of type list")

    for i in range(0, len(card_layouts)):
        if "heading_1" in card_layouts[i]:
            heading_1 = card_layouts[i]["heading_1"]
            if not isinstance(heading_1, basestring):
                raise RuntimeError(
                    "Input argument heading_1 should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["heading_1"] = ""

        if "heading_2" in card_layouts[i]:
            heading_2 = card_layouts[i]["heading_2"]
            if not isinstance(heading_2, basestring):
                raise RuntimeError(
                    "Input argument heading_2 should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["heading_2"] = ""

        if "heading_3" in card_layouts[i]:
            heading_3 = card_layouts[i]["heading_3"]
            if not isinstance(heading_3, basestring):
                raise RuntimeError(
                    "Input argument heading_3 should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["heading_3"] = ""

        if "icon" in card_layouts[i]:
            icon = card_layouts[i]["icon"]
            if not isinstance(icon, basestring):
                raise RuntimeError("Input argument icon should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["icon"] = ""

        if "footer" in card_layouts[i]:
            footer = card_layouts[i]["footer"]
            if not isinstance(footer, basestring):
                raise RuntimeError(
                    "Input argument footer should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["footer"] = ""

        if "user_click_actions" in card_layouts[i]:
            if not isinstance(card_layouts[i]["user_click_actions"], list):
                raise RuntimeError("Input argument footer should be of type list" + " | Element: " + unicode(i))

            user_click_actions = card_layouts[i]["user_click_actions"]

            if len(user_click_actions) > 0 and ("logo" not in user_click_actions[0] or "url" not in \
                                                user_click_actions[
                                                    0]):
                raise RuntimeError("Input argument user_click_actions is not a list of dicts of correct structure")
        else:
            card_layouts[i]["user_click_actions"] = []

    __log("Input arguments are okay", verbose)

    return api_responses.get_cards_api_response(title, card_layouts)


def get_table(theader, tbody, tlabel, verbose=False):
    """
    :param theader: The table header
    Example: {
        "column_0": "Type",
        "column_1": "Balance"
    }
    :param tbody: The table body
        {
            {
              "column_0": "<b>CL</b>",
              "column_1": "20.0"
            },
            {
              "column_0": "<b>PL</b>",
              "column_1": "20.0"
            },
            { ... }
            ...
        }
    :param tlabel: Label text for table card
    :param verbose: Set this to True to see debug messages
    :return: Table Layout Payload
    Raises exceptions for erroneous conditions
    """

    __log("Function send_card called with arguments:", verbose)
    __log("theader: " + unicode(theader), verbose)
    __log("tbody: " + unicode(tbody), verbose)

    if not isinstance(theader, dict):
        raise RuntimeError("Input argument theader should be of type dict")

    if not isinstance(tbody, list):
        raise RuntimeError("Input argument card_layout should be of type list")

    __log("Input arguments are okay", verbose)

    return api_responses.get_table_api_response(theader, tbody, tlabel)


if __name__ == '__main__':
   theader = {"column_0": "Name", "column_1": "Relationship"}
   tbody = [("Ram", "Father"), ("Seetha", "Mother")]
   print get_table(theader, tbody,"Here is your dependent details")
