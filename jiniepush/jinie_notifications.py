import json

import requests

from jiniepush import notifications
from jiniepush.common_logging import __log


# Use meeting instead of metg
def send_meeting_reminder_card(alt_id, greeting, meeting_title, meeting_time, meeting_loc, members, meeting_actions,
                                verbose=False):
    """

        :param alt_id: The Alt ID of the user.
        :param greeting: The Username for generic array  object.
        :param meeting_title: The title of meeting.
        :param meeting_time: The Time of meeting.
        :param meeting_loc: The Location of meeting.
        :param members: This is a list of dictionaries.
             {
                name: name of participant,
                id: email_id of participant
             }
        :param meeting_actions: This is a list of dictionaries.
             {
                "label": "Tentative",
                "val": "https://www.google.com"
             }
        :param verbose: Set this to True to see debug messages
        :return: True if sending was successful, False otherwise.
        Raises exceptions for erroneous conditions
    """
    __log("Function send_card called with arguments:", verbose)
    __log("alt_id: " + unicode(alt_id), verbose)
    __log("greeting: " + unicode(greeting), verbose)
    __log("metg_title: " + unicode(meeting_title), verbose)
    __log("metg_time: " + unicode(meeting_time), verbose)
    __log("metg_loc: " + unicode(meeting_loc), verbose)
    __log("members: " + unicode(members), verbose)
    __log("metg_actions: " + unicode(meeting_actions), verbose)

    if not isinstance(alt_id, basestring):
        raise RuntimeError("Input argument alt_id should be of type str")
    if not isinstance(greeting, basestring):
        raise RuntimeError("Input argument user_name should be of type str")
    if not isinstance(meeting_time, basestring):
        raise RuntimeError("Input argument metg_time should be of type str")
    if not isinstance(meeting_time, basestring):
        raise RuntimeError("Input argument metg_title should be of type str")
    if not isinstance(meeting_loc, basestring):
        raise RuntimeError("Input argument metg_loc should be of type str")
    for i in range(0, len(members)):
        if "name" in members[i]:
            name = members[i]["name"]
            if not isinstance(name, basestring):
                raise RuntimeError(
                    "Input argument name should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            members[i]["name"] = ""
        if "id" in members[i]:
            member_id = members[i]["id"]
            if not isinstance(member_id, basestring):
                raise RuntimeError(
                    "Input argument id should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            members[i]["id"] = ""

    for i in range(0, len(meeting_actions)):
        if "label" in meeting_actions[i]:
            action_label = meeting_actions[i]["label"]
            if not isinstance(action_label, basestring):
                raise RuntimeError(
                    "Input argument metg action label should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            meeting_actions[i]["label"] = ""
        if "val" in meeting_actions[i]:
            action_val = meeting_actions[i]["val"]
            if not isinstance(action_val, basestring):
                raise RuntimeError(
                    "Input argument metg action id should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            meeting_actions[i]["val"] = ""

    __log("Input arguments are okay", verbose)

    notification_body = notifications.get_meeting_reminder_card_layout(alt_id, greeting, meeting_title, meeting_time,
                                                                        meeting_loc, members, meeting_actions)

    __log("About to send notification:\n" + json.dumps(notification_body, indent=4, default=unicode), verbose)

    response = requests.post(url="https://messenger.peoplestrong.com/custom/jinieNotificationRequest",
                             headers={"Content-Type": "application/json",
                                      "Authorization": "JinieNotificationITGGN575"},
                             data=json.dumps(notification_body, separators=(',', ':')),
                             timeout=30)

    __log("Received response: " + json.dumps(response, indent=4, default=unicode), verbose)

    if response.status_code == 200:
        return True
    return False


def send_cards(alt_id, title, card_layouts, verbose=False):
    """

    :param alt_id: The Alt ID of the user.
    This is the Id present in the ejabberd table.
    Get it from the mobile team! messenger.mobile@peoplestrong.com
    :param title: The text appearing before the card layout, i.e. the narrative.
    Example: Hi xyz, here are the relevant candidates
    :param card_layout: This is a dictionary. The keys that need to be defined are:
        {
            heading_1: First text appearing in sequence in the card layout
            heading_2: Second text appearing in sequence in the card layout
            footer: Text appearing in the bottom of the card layout
            icon: The background image / icon to be displayed in the card layout
            actions: A list of dicts. Example:
            [{"logo": "https://abc.com/xyz.png", "url":"https://xyz.com"}]
            This will display an action button with the icon in "logo". On clicking the url
            will be opened in the browser
        }
    :param verbose: Set this to True to see debug messages
    :return: True if sending was successful, False otherwise.
    Raises exceptions for erroneous conditions
    """

    __log("Function send_card called with arguments:", verbose)
    __log("alt_id: " + unicode(alt_id), verbose)
    __log("title: " + unicode(title), verbose)
    __log("card_layout: " + unicode(card_layouts), verbose)

    if not isinstance(alt_id, basestring):
        raise RuntimeError("Input argument alt_id should be of type str")

    if not isinstance(title, basestring):
        raise RuntimeError("Input argument title should be of type str or unicode")

    if not isinstance(card_layouts, list):
        raise RuntimeError("Input argument card_layout should be of type list")

    for i in range(0, len(card_layouts)):
        if "heading_1" in card_layouts[i]:
            heading_1 = card_layouts[i]["heading_1"]
            if not isinstance(heading_1, basestring):
                raise RuntimeError(
                    "Input argument heading_1 should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["heading_1"] = ""

        if "heading_2" in card_layouts[i]:
            heading_2 = card_layouts[i]["heading_2"]
            if not isinstance(heading_2, basestring):
                raise RuntimeError(
                    "Input argument heading_2 should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["heading_2"] = ""

        if "heading_3" in card_layouts[i]:
            heading_3 = card_layouts[i]["heading_3"]
            if not isinstance(heading_3, basestring):
                raise RuntimeError(
                    "Input argument heading_3 should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["heading_3"] = ""

        if "icon" in card_layouts[i]:
            icon = card_layouts[i]["icon"]
            if not isinstance(icon, basestring):
                raise RuntimeError("Input argument icon should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["icon"] = ""

        if "footer" in card_layouts[i]:
            footer = card_layouts[i]["footer"]
            if not isinstance(footer, basestring):
                raise RuntimeError(
                    "Input argument footer should be of type str or unicode" + " | Element: " + unicode(i))
        else:
            card_layouts[i]["footer"] = ""

        if "user_click_actions" in card_layouts[i]:
            if not isinstance(card_layouts[i]["user_click_actions"], list):
                raise RuntimeError("Input argument footer should be of type list" + " | Element: " + unicode(i))

            user_click_actions = card_layouts[i]["user_click_actions"]

            if len(user_click_actions) > 0 and ("logo" not in user_click_actions[0] or "url" not in \
                                                user_click_actions[
                                                    0]):
                raise RuntimeError("Input argument user_click_actions is not a list of dicts of correct structure")
        else:
            card_layouts[i]["user_click_actions"] = []

    __log("Input arguments are okay", verbose)

    notification_body = notifications.get_cards_notification_layout(alt_id, title, card_layouts)

    __log("About to send notification:\n" + json.dumps(notification_body, indent=4, default=unicode), verbose)

    response = requests.post(url="https://altmessenger.peoplestrong.com/AltChatServer/jinieNotificationRequest/1.0",
                             headers={"Content-Type": "application/json"},
                             data=json.dumps(notification_body, separators=(',', ':')),
                             timeout=30)

    __log("Received response: " + json.dumps(response, indent=4, default=unicode), verbose)

    if response.status_code == 200:
        return True

    return False


def send_generic_design(alt_id, payload, verbose=False):
    """

    :param alt_id: The Alt ID of the user.
    This is the Id present in the ejabberd table.
    Get it from the mobile team! messenger.mobile@peoplestrong.com
    :param payload: The payload to render. This is the new stylish UI!
    :param verbose: Set this to True to see debug messages
    :return: True if sending was successful, False otherwise.
    Raises exceptions for erroneous conditions
    """

    __log("Function send_generic_design called with arguments:", verbose)
    __log("alt_id: " + unicode(alt_id), verbose)
    __log("payload: " + unicode(payload), verbose)

    if not isinstance(alt_id, basestring):
        raise RuntimeError("Input argument alt_id should be of type str")

    if not isinstance(payload, list):
        raise RuntimeError("Input argument card_layout should be of type list")

    __log("Input arguments are okay", verbose)

    notification_body = notifications.get_generic_design_layout(alt_id, payload)

    __log("About to send notification:\n" + json.dumps(notification_body, indent=4, default=unicode), verbose)

    response = requests.post(url="https://altmessenger.peoplestrong.com/AltChatServer/jinieNotificationRequest/1.0",
                             headers={"Content-Type": "application/json"},
                             data=json.dumps(notification_body, separators=(',', ':')),
                             timeout=30)

    __log("Received response: " + json.dumps(response, indent=4, default=unicode), verbose)

    if response.status_code == 200:
        return True

    return False


def send_batch_simple(alt_id_message_tup, verbose=False):
    pass


def send_simple(alt_id, message, format_message=True, verbose=False):
    """

    :param alt_id: The Alt ID of the user.
    This is the Id present in the ejabberd table.
    Get it from the mobile team! messenger.mobile@peoplestrong.com

    :param message: The message to send

    :param format_message: Beautify the message a bit - put in line breaks.
    Set to false if you want the message exactly to be displayed the way you want.

    :param verbose: Set this to True to see debug messages

    :return: True if sending was successful, False otherwise.
    Raises exceptions for erroneous conditions

    """
    __log("Function send_simple called with arguments (v12):", verbose)
    __log("alt_id: " + unicode(alt_id), verbose)
    __log("message: " + unicode(message), verbose)
    __log("format_message: " + unicode(format_message), verbose)
    __log("verbose: " + unicode(verbose), verbose)

    if not isinstance(alt_id, basestring):
        raise RuntimeError("Input argument alt_id should be of type str or unicode")

    if not isinstance(message, basestring):
        raise RuntimeError("Input argument message should be of type str or unicode")

    if message is None or len(message) == 0:
        raise RuntimeError("Input argument message is None or empty")

    __log("Input arguments are okay", verbose)

    if format_message:
        __log("Formatting message", verbose)
        message = message.strip()
        message = message + "<br/>"
        message = message.replace("\r\n", "\n")
        message = message.replace("\n", "<br/>")
        __log("Formatted message: " + message, verbose)

    notification_body = notifications.get_simple_notification_layout(alt_id, message)

    __log("About to send notification:\n" + json.dumps(notification_body, indent=4, default=unicode), verbose)

    response = requests.post(url="https://altmessenger.peoplestrong.com/AltChatServer/jinieNotificationRequest/1.0",
                             headers={"Content-Type": "application/json"},
                             data=json.dumps(notification_body, separators=(',', ':')),
                             timeout=30)

    __log("Received response: " + json.dumps(response, indent=4, default=unicode), verbose)

    if response.status_code == 200:
        return True

    return False


if __name__ == "__main__":
    send_meeting_reminder_card("2721489", "Edukondalu", "test meeting", "10: 15AM - 10: 30AM", "AltifyHR", [
        {
            "name": "Daksh Gargas",
            "id": "daksh.gargas@peoplestrong.com"
        },
        {
            "name": "Mukesh Antil",
            "id": "abhishek.yadav@peoplestrong.com"
        }],
                                [{
                                    "label": "Open in outlook",
                                    "val": "https://www.outlook.com"
                                },
                                    {
                                        "label": "Tentative",
                                        "val": "https://www.google.com"
                                    }], True)
    """
    send_simple("2205395", "a")
    # print send_simple("2205395", "Hello, \n This is a test message!")
    print send_cards("2205395", "Hi Aakash, below are the recommended courses", [
        {
            "icon": "http://daas.peoplestrong.com/static/images/ps-capabiliti-learning-logo_v6.jpg",
            "heading_1": "C# Basics for Advanced",
            "heading_2": "Rating: 4.5",
            "footer": "Intermediate Level",
            "user_click_actions": [
                {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                 "url": "https://altlearning.capabiliti.co/index.php"}],
        },
        {
            "icon": "http://daas.peoplestrong.com/static/images/udemy.png",
            "heading_1": "C# Basics for Beginners",
            "heading_2": "Rating: 4.5",
            "footer": "All Levels",
            "user_click_actions": [
                {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                 "url": "https://www.udemy.com/machinelearning/"}],
        }
    ], verbose=True)
    """
