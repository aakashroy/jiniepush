import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="jiniepush",
    version="0.0.1",
    author="Aakash Roy",
    author_email="aakash.roy@peoplestrong.com",
    description="A wrapper for sending Notifications to Peoplestrong Alt Jinie",
    long_description=long_description,
    url="https://bitbucket.org/peoplestrongalt",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: Other/Proprietary License",
        "Operating System :: OS Independent",
        "Development Status :: 4 - Beta"
    ],
)
